<?php
function import($country,$countryCode,$currencyCode){
	
  if($country == 'spain'){
	 $cities = array("Aguilar De La Frontera","Alba De Tormes","Albarracín","Alcalá La Real","Alcañiz","Alcaudete","Alcázar De San Juan","Alhambra","Almagro","Almuñécar","Andorra","Andújar","Arcos De La Frontera","Arévalo","Arriate","Arzúa","Avila","Azaila","Baena","Baeza","Baiona","Barcelona","Begur","Belchite","Besalú","Bilbao","Burgos","Cabra","Caceres","Cadaqués","Calaceite","Calatayud","Campo De Criptana","Cangas De Onís","Cardona","Cariñena","Carmona","Caseres","Castellfollit de la Roca","Ciudad Real","Colindres","Coll De Nargó","Consuegra","Cordoba","Cuenca","Daimiel","Daroca","El Escorial","El Puente Del Arzobispo","El Toboso","Falset","Fernán-Núñez","Fuendetodos","Fuengirola","Fuentes De Jiloca","Gandesa","Girona","Granada","Guadalupe","Hondarribia","Illescas","Italica","Jarandilla De La Vera","Javier","Jerez De La Frontera","La Alberca","La Coruña","La Estrella","La Toba","Lanjarón","Leon","Llafranc","Lloret De Mar","Logroño","Lucena","L’Escala","Madrid","Madrigal De Las Altas Torres","Malága","Maluenda","Manresa","Marbella","Martorell","Medina Del Campo","Melide","Mérida","Molina De Aragón","Mombuey","Monistrol De Calders","Montecorto","Montilla","Montón","Montoro","Monturque","Nerja","Nuévalos","Olite","Olot","Orense","Organyà","Orgiva","Oropesa","Pamplona","Panes","Pedraza de la Sierra","Pozuel Del Campo","Puebla De Sanabria","Puerto Banús","Puerto Lápice","Puigcerdà","Ribes De Freser","Rincón De La Victoria","Ripoli","Ronda","Roses","Royuela","Rupit","Salamanca","Salobreña","Santander","Santiago de Compostela","Santillana Del Mar","Santo Domingo De La Calzada","Segovia","Seu d’Urgell","Seville","Solsona","Sos Del Rey Católico","S’Agaró","Tarancón","Teruel","Toledo","Tordesillas","Toro","Torremolinos","Tossa De Mar","Trujillo","Ubeda","Uña","Valdepeñas","Velilla","Zafra","Zahara","Zamora");
   }else if($country == 'portugal'){
	  $cities = array("Abrantes","Agualva-Cacém","Águeda","Albufeira","Alcácer do Sal","Alcobaça","Almada","Almeirim","Alverca do Ribatejo","Amadora","Amarante","Amora","Anadia","Angra do Heroísmo","Aveiro","Barcelos","Barreiro","Beja","Braga","Bragança","Caldas da Rainha","Câmara de Lobos","Caniço","Cantanhede","Cartaxo","Castelo Branco","Chaves","Coimbra","Costa da Caparica","Covilhã","Elvas","Entroncamento","Ermesinde","Esmoriz","Espinho","Esposende","Estarreja","Estremoz","Évora","Fafe","Faro","Fátima","Felgueiras","Figueira da Foz","Fiães","Freamunde","Funchal","Fundão","Gafanha da Nazaré","Gandra","Gondomar","Gouveia","Guarda","Guimarães","Horta","Ílhavo","Lagoa","Lagos","Lamego","Leiria","Lisbon","Lixa","Loulé","Loures","Lourosa","Macedo de Cavaleiros","Maia","Mangualde","Marco de Canaveses","Marinha Grande","Matosinhos","Mealhada","Mêda","Miranda do Douro","Mirandela","Montemor-o-Novo","Montijo","Moura","Odivelas","Olhão da Restauração","Oliveira de Azeméis","Oliveira do Bairro","Oliveira do Hospital","Ourém","Ovar","Paços de Ferreira","Paredes","Penafiel","Peniche","Peso da Régua","Pinhel","Pombal","Ponta Delgada","Ponte de Sor","Portalegre","Portimão","Porto","Vila Baleira","Póvoa de Santa Iria","Póvoa de Varzim","Praia da Vitória","Quarteira","Queluz","Rebordosa","Reguengos de Monsaraz","Ribeira Grande","Rio Maior","Rio Tinto","Sabugal","Sacavém","Santa Comba Dão","Santa Cruz","Santa Maria da Feira","Santana","Santarém","Santiago do Cacém","Santo Tirso","São João da Madeira","São Mamede de Infesta","São Salvador de Lordelo","Seia","Seixal","Serpa","Setúbal","Silves","Sines","Tarouca","Tavira","Tomar","Tondela","Torres Novas","Torres Vedras","Trancoso","Trofa","Valbom","Vale de Cambra","Valongo","Valpaços","Vendas Novas","Viana do Castelo","Vila do Conde","Vila Franca de Xira","Vila Nova de Famalicão","Vila Nova de Foz Côa","Vila Nova de Gaia","Vila Nova de Santo André","Vila Real","Vila Real de Santo António","Viseu","Vizela");
   }	
	$selectedCountry = $country;
	
	$apiKey = '2kmwxzue3m8uqwk2y8a3xca7';  
	$secret = '7kES7yqk';  
	$timestamp = gmdate('U'); // 1200603038  (Thu, 17 Jan 2008 20:50:38 +0000)   
	$sig = md5($apiKey . $secret . $timestamp);
 
   //use latest minorRev 14
  $url  ='http://api.eancdn.com//ean-services/rs/hotel/v3/list?minorRev=25';
  $url .= '&cid=55505';
  $url .= '&apiKey=2kmwxzue3m8uqwk2y8a3xca7';
  $url .= '&locale=en_US';
  $url .= '&currencyCode='.$currencyCode;
  $url .= '&type=json';
  $url .= '&sig='.$sig;
//using the cache returns results much faster
  $url .= '&supplierCacheTolerance=MED_ENHANCED';
//dates and occupancy
// $url .='&arrivalDate=09/04/2012&departureDate=09/05/2012&room1=2';

foreach($cities as $city){

 $XML = "&xml=<HotelListRequest><city>".$city."</city><countryCode>".$countryCode."</countryCode><RoomGroup><Room><numberOfAdults>1</numberOfAdults><numberOfChildren>0</numberOfChildren></Room></RoomGroup></HotelListRequest>";  

  $header[] = "Accept: application/json";
  $header[] = "Accept-Encoding: gzip";

echo    $url.$XML; die();
    $ch = curl_init();
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	curl_setopt($ch,CURLOPT_ENCODING , "gzip");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch,CURLOPT_POSTFIELDS,$XML);
	curl_setopt($ch, CURLOPT_URL, $url.$XML );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    $response = json_decode(curl_exec($ch));
	curl_close($ch);
  
    $my = objectToArray($response);
    $my1 = array();
    $my1 = $my[HotelListResponse][HotelList][HotelSummary];
   
  // echo '<pre>'; print_r($my1); echo '</pre>'; die("in die");
    
    $hotelId='';$hotelName='';$address='';$city='';$country='';$postalCode='';$hotelRating='';$confidenceRating='';$tripRating='';$tripReviewCount='';$tripReviewUrl='';$latitude='';$longitude='';$thumbnail='';
    
   
    if(!empty($my1) && count($my1)>0){
	  	foreach($my1 as $res){
		  global $con,$resp;		
		  $hotelId     		=  $res[hotelId] != '' ? $res[hotelId] : 'N/A';
		  $hotelName   		=  $res[name] != '' ? mysql_real_escape_string($res[name]) : 'N/A';
		  $address    		=  $res[address1] != '' ? mysql_real_escape_string($res[address1]) : 'N/A';
		  $city       		=  $res[city] != '' ? $res[city] : 'N/A'; 
		  $postalCode  		=  $res[postalCode] != '' ? $res[postalCode] : 'N/A'; 
		  $hotelRating 		=  $res[hotelRating] != '' ? $res[hotelRating] : 'N/A';
		  $confidenceRating =  $res[confidenceRating] != '' ? $res[confidenceRating] : 'N/A';
		  $tripRating  		=  $res[tripAdvisorRating] != '' ? $res[tripAdvisorRating] : 'N/A';
		  $tripReviewCount 	=  $res[tripAdvisorReviewCount] != '' ? $res[tripAdvisorReviewCount] : 'N/A';
		  $tripReviewUrl  	=  $res[tripAdvisorRatingUrl] != '' ? $res[tripAdvisorRatingUrl] : 'N/A';
		  $latitude  		=  $res[latitude] != '' ? $res[latitude] : 'N/A';
		  $longitude 		=  $res[longitude] != '' ? $res[longitude] : 'N/A';
		  $thumbnail 		=  $res[thumbNailUrl] != '' ? $res[thumbNailUrl] : 'N/A';
		  $todays_date      = date("Y-m-d h:i:s");
			
		// ********* Condition to check for already present in table **********// 	
		    $checkCount = "select hotel_name from tbl_hotels where hotel_id =".$hotelId."";
			$response = mysql_query($checkCount, $con);
			$num_rows = @mysql_num_rows($response);
		
			if($num_rows <= 0){
			        mysql_query('SET NAMES utf8'); 
					mysql_query('SET CHARACTER_SET utf8'); 
					mysql_query("SET COLLATION_CONNECTION = 'utf-8'");
					mysql_query("START TRANSACTION");
				    $query = 'INSERT INTO tbl_hotels(`hotel_id`,`hotel_name`,`hotel_address`,`hotel_city`,`hotel_country`,`hotel_postal_code`,`hotel_rating`,`hotel_confidence_rating`,`trip_advisor_rating`,`trip_advisor_review_count`,`trip_advisor_review_url`,`hotel_latitude`,`hotel_longitude`,`hotel_thumbnail_url`,`created_date`) values ("'.$hotelId.'","'.$hotelName.'","'.$address.'","'.$city.'","'.$selectedCountry.'","'.$postalCode.'","'.$hotelRating.'","'.$confidenceRating.'","'.$tripRating.'","'.$tripReviewCount.'","'.$tripReviewUrl.'","'.$latitude.'","'.$longitude.'","'.$thumbnail.'","'.$todays_date.'")';
	
			        $retval = mysql_query($query, $con);
				   if($retval) {
						 mysql_query("COMMIT");
					}else{        
						mysql_query("ROLLBACK");
					}
					echo "Entered data successfully\n";
					echo "<br/>";
		    }
		}
	}
  }	
}	
?>
