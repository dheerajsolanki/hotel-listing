<?php
/*
 *   FileName    : holiday_check.php
 *
 *   FilePurpose : Scrap the data from holidaycheck.com
 *
 *   Created By  :  SIPL
 *
 */

include("./includes/inc_config.php");

function getRequest($outputFormat,$partnerToken,$countryId,$countryName){
    header("Content-Type:text/plain");
    // example for a filter
    $filter = "?filter=countryId:".$countryId.""; // Need to change country id here as well //
    // example for a sort
    $sort = "&sort=averageRating:desc";
    //Initialize the curl session
    $ch = curl_init();
    //set url for the api
    $url ="https://api.holidaycheck.com/v2/hotel" . $filter . $sort . "&partner_token=" . $partnerToken;  
    // setting the Header for the request
    $_header = array();
		array_push($_header, "Accept: text/" . $outputFormat);
		array_push($_header, "Content-Type: text/" . $outputFormat);
    curl_setopt($ch,CURLOPT_HTTPHEADER,$_header);
    curl_setopt($ch,CURLOPT_HTTPGET,true);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE); 
	curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch,CURLOPT_CAINFO, "http://183.182.84.84:/hotel_listing/mozilla.pem");
    $store = curl_exec($ch);
     if(curl_errno($ch)){
		echo 'Curl error: ' . curl_error($ch);
	  }
	 
	curl_close($ch);
	$elem = new SimpleXMLElement($store);	
	$total = $elem->total;  
	// ******* Logic Area for setting offset and limit *********//
	$laps = ceil($total/100); 
	for($i=1;$i<=$laps;$i++){
	    $offset = '&offset='.(($i*100) - 100);	
			// example for a filter
			$filter_new = "?filter=countryId:".$countryId.""; // Need to change country id here as well //
			// example for a sort
			$sort_new = "&sort=averageRating:desc&limit=100".$offset;
			//Initialize the curl session
			$ch = curl_init();
			//set url for the api
		 $url ="https://api.holidaycheck.com/v2/hotel". $filter_new . $sort_new . "&partner_token=" . $partnerToken; 
		 test($url,$outputFormat);
	}	
	// ******* Logic Area for setting offset and limit *********//
}


$query = "select `key`,`value` from tbl_holiday_country";
$result = mysql_query($query,$con);
  while($row = mysql_fetch_array($result)){
       getRequest("xml","10833A0B-2E83-42C5-B5C2-DE22F7BBCB92",$row['key'],$row['value']);
   }

   
 function test($url,$outputFormat){
	$newUrl = strip_tags($url);
	global $con,$resp;
	$_header = array();
			array_push($_header, "Accept: text/" . $outputFormat);
			array_push($_header, "Content-Type: text/" . $outputFormat);
	         
		    $ch = curl_init();
			curl_setopt($ch,CURLOPT_HTTPHEADER,$_header);
			curl_setopt($ch,CURLOPT_HTTPGET,true);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_URL, $newUrl);
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch,CURLOPT_CAINFO, "http://183.182.84.84:/hotel_listing/mozilla.pem");
			$store = curl_exec($ch);
			 if(curl_errno($ch)){
				echo 'Curl error: ' . curl_error($ch);
			  }
			curl_close($ch);
			$elem = simplexml_load_string($store);
		  foreach ( $elem->items->hotel as $ach){
                	$address='';$city='';$selectedCountry='Egypt';$postalCode='';$hotelRating='';$confidenceRating='';$hcRating='';$tripReviewCount='';$hcReviewUrl='';$latitude='';$longitude='';$thumbnail='';
                        $todays_date = date("Y-m-d h:i:s");
		        $hcurl = $ach->hcUrl;
                        $tot = explode("/",$hcurl);
                        $hotelEncId = $tot[count($tot) -1];
                        $customUrl = "http://www.holidaycheck.com/services/snippet-service/V1/chunk/com.holidaycheck.app.map.NearbyBubbleApc/".$hotelEncId."/json";
		        $ch2 = curl_init();
			curl_setopt($ch2,CURLOPT_URL, $customUrl);
			curl_setopt($ch2,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch2,CURLOPT_HTTPGET,true);
			curl_setopt($ch2,CURLOPT_SSL_VERIFYPEER, FALSE); 
			curl_setopt($ch2,CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch2,CURLOPT_CAINFO, "http://183.182.84.84:/hotel_listing/mozilla.pem");
			 $storeDetail = curl_exec($ch2);
			 if(curl_errno($ch2)){
				echo 'Curl error: ' . curl_error($ch2);
			  }
			curl_close($ch2);
		    $relDat = json_decode($storeDetail);
		    $hotelName = $relDat->name;
		    $longitude =  $relDat->location[0];
		    $latitude =  $relDat->location[1];
		    $starRating = $relDat->stars;
		    $hcReviewCount = $relDat->reviews->count;
		    $hotelId =  $ach->id;
                    $hotelName = $ach->name;
		    $hcRating = $relDat->rating;
                    $hcurl = $ach->hcUrl;
		    $hcReviewUrl = $ach->hcReviewsUrl;
		    $hcrateurl = $ach->hcRateUrl; 
		    
                    $checkCount = "select hotel_name from tbl_holiday_check where hotel_id =".$hotelId.""; 
		    $response = mysql_query($checkCount, $con);
			$num_rows = @mysql_num_rows($response);
			if($num_rows <= 0){
                                        mysql_query('SET NAMES utf8'); 
					mysql_query('SET CHARACTER_SET utf8'); 
					mysql_query("SET COLLATION_CONNECTION = 'utf-8'");
					mysql_query("START TRANSACTION");
                                        $query = 'INSERT INTO tbl_holiday_check(`hotel_id`,`hotel_name`,`hotel_address`,`hotel_city`,`hotel_country`,`hotel_postal_code`,`hotel_check_star_rating`,`hotel_confidence_rating`,`holiday_check_rating`,`holiday_check_review_count`,`holiday_check_review_url`,`holiday_check_rate_url`,`hotel_latitude`,`hotel_longitude`,`hotel_thumbnail_url`,`created_date`) values ("'.$hotelId.'","'.$hotelName.'","'.$address.'","'.$city.'","'.$selectedCountry.'","'.$postalCode.'","'.$starRating.'","'.$confidenceRating.'","'.$hcRating.'","'.$hcReviewCount.'","'.$hcReviewUrl.'","'.$hcrateurl.'","'.$latitude.'","'.$longitude.'","'.$thumbnail.'","'.$todays_date.'")';
                                        $retval = mysql_query($query, $con);
                                        if($retval) {
						 mysql_query("COMMIT");
					}else{        
						mysql_query("ROLLBACK");
					}
					echo "Entered data successfully\n";
					echo "\n";   
                        }
                   }
  }  
//************ END OF FILE **********************//
?>