<?php
require_once 'jq-config.php';
// include the jqGrid Class
require_once "jqGrid.php";

// include the PDO driver class
require_once "jqGridPdo.php";

// Connection to the server
$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);
//$conn = new PDO("localhost;country","root","");
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT id, hotel_id, hotel_name, hotel_address FROM tbl_hotels';

$grid->table = 'tbl_hotels';
$grid->setPrimaryKeyId('id');
$grid->serialKey = false;
// set the ouput format to json
$grid->dataType = 'json';
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('myfirstgrid.php');
// Set grid caption using the option caption
$grid->setGridOptions(array(
    "caption"=>"User Details",
    "rowNum"=>10,
    "sortname"=>"user_id",
    //"hoverrows"=>true,
    //"rowList"=>array(10,20,50),
    ));
// Change some property of the field(s)
$grid->setColProperty("user_id", array("editrules"=>array("required"=>true)));
$grid->navigator = true;
$grid->setNavOptions('navigator', array("excel"=>false,"add"=>true,"edit"=>true,"del"=>true,"view"=>true, "search"=>true));
//$grid->setNavOptions('edit', array("height"=>110,"dataheight"=>"auto"));
$grid->setNavOptions('add', array("editrules"=>array("required"=>true))); 
// Enjoy
$grid->renderGrid('#grid','#pager',true, null, null, true, true);
$conn = null;
?>
