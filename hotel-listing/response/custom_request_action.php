<?php
/*
 *   FileName    : custom_request_action.php
 *
 *   FilePurpose : Read the xml request perform search operation and return result in XML
 *
 *   Created By  :  SIPL
 *
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Custom Request Response For Hotel Rating</title>
	<meta http-equiv="content-type" />
	<meta name="generator" content="Geany 0.21" />
</head>
<body>
<?php
ini_set('display_errors', 'On');
ini_set('allow_url_fopen ','ON');
ini_set('upload_max_filesize', '200M'); 
ini_set('max_execution_time', '30000');
ini_set('meemory_limit', '96M');
ini_set('post_max_size', '64M');

// Defining database object globally // 
  //global $con,$res;

// Connet to DB //
 // $con = @mysql_connect("localhost","root","sipl@1234") or die('Error :'.mysql_error());

// Select DB //
  //$res = mysql_select_db("hotel_listing",$con) or die('Error :'.mysql_error());
  
  include('../includes/inc_config.php');

// Taking xml data posted by form //
  $a = $_POST['xmldata']; 
  if($a == ''){
	 echo "No Input Found, Please Try Again";  
         $backURL= $_SERVER['SERVER_NAME'].'/'.'hotel-listing/response/custom_request_input.php';
         echo "<a href='".$backURL."'>Go Back</a>";	 
	 die();
  }
	
// Defining header of file //
 header ("Content-Type:text/xml"); 
 
 $xml = simplexml_load_string ($a);
 if ($xml === false) {
       echo "Failed loading XML\n";
        foreach(libxml_get_errors() as $error) {
            echo "\t ERROR :", $error->message;
        }
 }

 if(!empty($xml)){
     // Taking request parameter from XML file //
		$name = utf8_decode($xml->nameQuery);
		$lat = $xml->geolocation->latitude;
		$long = $xml->geolocation->longitude;
		$radius = $xml->searchRadius;
	
	// Calling functions to read from DB // 
	ob_clean(); 
	$resultxml = '<searchRatingResponse>';
	$data['holiday'] = getXMLData($name,$lat,$long,$radius,'tbl_holiday_check');
	if(!empty($data['holiday'])){
                               $textHoliday = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['holiday']['hotel_name']);
				$hrating =utf8_decode($data['holiday']['rating']);
				$hrcount = $data['holiday']['reviewcount'];
				$avghRating = 0;
				 if(is_numeric($data['holiday']['rating'])){
				  $avghRating = (($data['holiday']['rating'] * 10)/$data['holiday']['ratingScale']);
				 }
				 $hhcount = 0;
				 if(is_numeric($data['holiday']['reviewcount'])){
				  $hhcount = $data['holiday']['reviewcount'];
				 }
				$resultxml .= '<system name="holidaycheck">'; 
				$resultxml .= '<hotelName>'.utf8_encode($textHoliday).'</hotelName>'; 
				$resultxml .= '<hotelId>'.$data['holiday']['hotel_id'].'</hotelId>'; 
				$resultxml .= '<geolocation>';
				$resultxml .= '<latitude>'.$data['holiday']['latitude'].'</latitude>'; 
				$resultxml .= '<longitude>'.$data['holiday']['Longitude'].'</longitude>'; 
				$resultxml .= '</geolocation>';
				$resultxml .= '<rating>'.$data['holiday']['rating'].'</rating>'; 
				$resultxml .= '<ratingScale>'.$data['holiday']['ratingScale'].'</ratingScale>'; 
				$resultxml .= '<reviewCount>'.$data['holiday']['reviewcount'].'</reviewCount>'; 
				$resultxml .= '<reviewUrl>'.$data['holiday']['reviewurl'].'</reviewUrl>'; 
				$resultxml .= '<lastUpdated>'.$data['holiday']['date'].'</lastUpdated>';
				$resultxml .= '</system>'; 
		}else{
				$hrating = '';
				$hrcount = '';
				$avghRating = 0;
				$hhcount = 0;
				$resultxml .= '<system name="holidaycheck">';
				$resultxml .= '';	
				$resultxml .= '</system>';	
		}		
		
	$data['trip'] = getXMLData($name,$lat,$long,$radius,'tbl_trip_hotels');   
	if(!empty($data['trip'])){
			   $textTrip = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['trip']['hotel_name']); 
                            $trating = $data['trip']['rating'];
			    $trcount = $data['trip']['reviewcount'];
			    $avgtrRating = 0;
			    if(is_numeric($data['trip']['rating'])){
			       $avgtrRating = (($data['trip']['rating'] * 10) /$data['trip']['ratingScale']);
			    } 
			    $ttcount = 0;
			    if(is_numeric($data['trip']['reviewcount'])){
			       $ttcount = $data['trip']['reviewcount'];
			    }  
				$resultxml .= '<system name="tripadvisor">'; 
				$resultxml .= '<hotelName>'.utf8_encode($textTrip).'</hotelName>'; 
				$resultxml .= '<hotelId>'.$data['trip']['hotel_id'].'</hotelId>'; 
				$resultxml .= '<geolocation>';
				$resultxml .= '<latitude>'.$data['trip']['latitude'].'</latitude>'; 
				$resultxml .= '<longitude>'.$data['trip']['Longitude'].'</longitude>'; 
				$resultxml .= '</geolocation>';
				$resultxml .= '<rating>'.$data['trip']['rating'].'</rating>'; 
				$resultxml .= '<ratingScale>'.$data['trip']['ratingScale'].'</ratingScale>'; 
				$resultxml .= '<reviewCount>'.$data['trip']['reviewcount'].'</reviewCount>'; 
				$resultxml .= '<reviewUrl>'.$data['trip']['reviewurl'].'</reviewUrl>'; 
				$resultxml .= '<lastUpdated>'.$data['trip']['date'].'</lastUpdated>';
				$resultxml .= '</system>'; 
	}else{
		        $trating = '';
			    $trcount = '';
			    $avgtrRating = 0;
			    $ttcount = 0;
		        $resultxml .= '<system name="tripadvisor">';
		        $resultxml .= '';	
				$resultxml .= '</system>';	 
	}
		
	$data['booking'] = getXMLData($name,$lat,$long,$radius,'tbl_booking_com');
        if(!empty($data['booking'])){
              $text=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['booking']['hotel_name']);
			    $brating = $data['booking']['rating'];
			    $bcount = $data['booking']['reviewcount'];
			    $avgbRating = 0;
			    if(is_numeric($data['booking']['rating'])){
			      $avgbRating = (($data['booking']['rating']*10 )/$data['booking']['ratingScale']);
			    } 
			    $bbcount = 0;
			    if(is_numeric($data['booking']['reviewcount'])){
					 $bbcount = $data['booking']['reviewcount'];
				}	
			     
				$resultxml .= '<system name="booking">'; 
				$resultxml .= '<hotelName>'. utf8_encode($text).'</hotelName>'; 
				$resultxml .= '<hotelId>'.$data['booking']['hotel_id'].'</hotelId>'; 
				$resultxml .= '<geolocation>';
				$resultxml .= '<latitude>'.$data['booking']['latitude'].'</latitude>'; 
				$resultxml .= '<longitude>'.$data['booking']['Longitude'].'</longitude>'; 
				$resultxml .= '</geolocation>';
				$resultxml .= '<rating>'.$data['booking']['rating'].'</rating>'; 
				$resultxml .= '<ratingScale>'.$data['booking']['ratingScale'].'</ratingScale>'; 
				$resultxml .= '<reviewCount>'.$data['booking']['reviewcount'].'</reviewCount>'; 
				$resultxml .= '<reviewUrl>'.$data['booking']['reviewurl'].'</reviewUrl>'; 
				$resultxml .= '<lastUpdated>'.$data['booking']['date'].'</lastUpdated>';
				$resultxml .= '</system>'; 
	}else{
		        $brating = '';
			    $bcount = '';
			    $avgbRating = 0;
			    $bbcount = 0;
		        $resultxml .= '<system name="booking">';
		        $resultxml .= '';	
				$resultxml .= '</system>';	 
	}

		  
// **** Case for combined rating from result ******//  
		/* $orignalRate = ($avgtrRating + $avghRating + $avgbRating)/3;
		 if(!is_numeric($orignalRate)){
			 $orignalRate ='';
			}*/ 
		
		if(is_numeric($hrating) && is_numeric($trating) && is_numeric($brating)){   
		   $orignalRate = ((($hrating * 10)/6.0)+ (($trating * 10)/5.0) + (($brating * 10)/10.0))/3;
		}else if(is_numeric($hrating) && is_numeric($trating) && !is_numeric($brating)){
			$orignalRate = ((($hrating * 10)/6.0)+ (($trating * 10)/5.0))/2;
		}else if(!is_numeric($hrating) && is_numeric($trating) && is_numeric($brating)){
			$orignalRate = ((($trating * 10)/5.0)+ (($brating * 10)/10.0))/2;
		}else if(is_numeric($hrating) && !is_numeric($trating) && is_numeric($brating)){
			$orignalRate = ((($hrating * 10)/6.0)+ (($brating * 10)/10.0))/2;
		}else if(is_numeric($hrating) && !is_numeric($trating) && !is_numeric($brating)){
			$orignalRate = (($hrating * 10)/6.0);
		}else if(!is_numeric($hrating) && is_numeric($trating) && !is_numeric($brating)){
			$orignalRate = (($trating * 10)/5.0);
		}else if(!is_numeric($hrating) && !is_numeric($trating) && is_numeric($brating)){
			$orignalRate = (($brating * 10)/10.0);
		}else if(!is_numeric($hrating) && !is_numeric($trating) && !is_numeric($brating)){
			$orignalRate =  '';
		}	
// **** Close Case for combined rating from result ******//  			

// **** Case for combined review count from result ******//  
		$totalCount = ($hhcount + $ttcount + $bbcount);
		if(!is_numeric($totalCount)){
			$totalCount ='';
		}	
		/*if(is_numeric($hrcount) && is_numeric($trcount)){   
		   $totalCount = ($hrcount + $trcount);
		}else if(is_numeric($hrcount) && !is_numeric($trcount)){
			$totalCount = $hrcount;
		}else if(!is_numeric($hrcount) && is_numeric($trcount)){
			$totalCount = $trcount;
		}else if(!is_numeric($hrcount) && !is_numeric($trcount)){
			$totalCount ='';
		}*/

		
		   //$totalCount =  ($hrcount + $trcount);
// **** Close Case for combined review count from result ******//		 
                	$resultxml .= '<system name="combined">'; 
			$resultxml .= '<rating>'.$orignalRate.'</rating>'; 
			$resultxml .= '<holidaylatitude>'.$data['holiday']['latitude'].'</holidaylatitude>'; 
			$resultxml .= '<holidaylongitude>'.$data['holiday']['Longitude'].'</holidaylongitude>'; 
			$resultxml .= '<triplatitude>'.$data['trip']['latitude'].'</triplatitude>'; 
			$resultxml .= '<triplongitude>'.$data['trip']['Longitude'].'</triplongitude>'; 
			$resultxml .= '<triplatitude>'.$data['booking']['latitude'].'</triplatitude>'; 
			$resultxml .= '<triplongitude>'.$data['booking']['Longitude'].'</triplongitude>'; 
			$resultxml .= '<ratingScale>10</ratingScale>'; 
			$resultxml .= '<reviewCount>'.$totalCount.'</reviewCount>'; 
			$resultxml .= '</system>';    
			$resultxml .= '</searchRatingResponse>';
                        //$resultxml = html_entity_decode($resultxml);
       // echo 'herer'. $resultxml; die();  
           $dom = new DOMDocument;
	   $dom->preserveWhiteSpace = FALSE;
	   $dom->loadXML($resultxml);
	   $dom->formatOutput = TRUE;
	  echo $dom->saveXml();
       	die();
 }
 
   function getXMLData($name,$lat,$long,$radius,$tbl){
	global $con,$res;
        $getData = '';
	    if($tbl == 'tbl_holiday_check'){
		 $getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`holiday_check_rating` as rating,`holiday_check_review_count` as reviewcount,`holiday_check_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' Where LOWER(`hotel_name`) Like Lower("%'.$name.'%") HAVING distance <='.$radius.' ORDER BY distance desc';
             //  $getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`holiday_check_rating` as rating,`holiday_check_review_count` as reviewcount,`holiday_check_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' Where LOWER(`hotel_name`) Like Lower("%'.$name.'%")';
    
           $response = mysql_query($getData);
           $holiday_data = array();
           $check  = mysql_num_rows($response);
           if($check > 0){
			   while($row = mysql_fetch_array($response)){
				   $id = $row['hotel_id'] != '' ? $row['hotel_id'] : 'N/A' ;
				   $name = $row['hotel_name'] != '' ? $row['hotel_name'] : 'N/A';
				   $Lat = $row['hotel_latitude'] != '' ? $row['hotel_latitude'] : 'N/A';
				   $Long = $row['hotel_longitude'] != '' ? $row['hotel_longitude'] : 'N/A';
				   $rate = $row['rating'] != '' ? $row['rating'] : 0;
				   $reviewCount = $row['reviewcount'] != '' ? $row['reviewcount'] : 0;
				   $reviewUrl = $row['reviewurl'] != '' ? $row['reviewurl'] : 'N/A';
				   $ratingScale = '6.0';
				   $date = date('Y-m-d\TH:i:sP',strtotime($row['created_date']));
				   $holiday_data = array(
										  'hotel_id'=>$id,
										  'hotel_name'=>$name,
										  'latitude'=>$Lat,
										  'Longitude'=>$Long,
										  'rating'=>$rate,
										  'reviewcount'=>$reviewCount,
										  'reviewurl'=>$reviewUrl,
										  'ratingScale'=>$ratingScale,
										  'date'=>$date
										);
				}
			}else{
				    $holiday_data = array();
			}	
			return $holiday_data;
        }else if($tbl == 'tbl_trip_hotels'){
	$trip_data = array();
	$getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`trip_advisor_rating` as rating,`trip_advisor_review_count` as reviewcount,`trip_advisor_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' Where `hotel_name` Like "%'.$name.'%" HAVING distance <='.$radius.' ORDER BY distance desc';
        //$getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`trip_advisor_rating` as rating,`trip_advisor_review_count` as reviewcount,`trip_advisor_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' LOWER(`hotel_name`) Like Lower("%'.$name.'%")';
          $response = mysql_query($getData);
          $check  = mysql_num_rows($response);
            if($check > 0){
			   while($row = mysql_fetch_array($response)){
				  $id = $row['hotel_id'] != '' ? $row['hotel_id'] : 'N/A' ;
				   $name = $row['hotel_name'] != '' ? $row['hotel_name'] : 'N/A';
				   $Lat = $row['hotel_latitude'] != '' ? $row['hotel_latitude'] : 'N/A';
				   $Long = $row['hotel_longitude'] != '' ? $row['hotel_longitude'] : 'N/A';
				   $rate = $row['rating'] != '' ? $row['rating'] : 0;
				   $reviewCount = $row['reviewcount'] != '' ? $row['reviewcount'] : 0;
				   $reviewUrl = $row['reviewurl'] != '' ? $row['reviewurl'] : 'N/A';
				   $ratingScale = '5.0';
                                   $date = date('Y-m-d\TH:i:sP',strtotime($row['created_date'])); 
				   $trip_data = array(
                                        		'hotel_id'=>$id,
							'hotel_name'=>$name,
							'latitude'=>$Lat,
							'Longitude'=>$Long,
							'rating'=>$rate,
							'reviewcount'=>$reviewCount,
							'reviewurl'=>$reviewUrl,
							'ratingScale'=>$ratingScale,
							'date'=>$date
                                                    );
                            }									
	}else{
	       $trip_data = array();
	}	
	return $trip_data;
      }else if($tbl == 'tbl_booking_com'){
			$booking_data = array();
		 	$getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`hotel_booking_rating` as rating,`hotel_booking_review_count` as reviewcount,`hotel_booking_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' Where `hotel_name` Like "%'.$name.'%" HAVING distance <='.$radius.' ORDER BY distance desc';
                 // $getData = 'select `hotel_id`,`hotel_name`,`hotel_address`,`hotel_latitude`,`hotel_longitude`,`hotel_booking_rating` as rating,`hotel_booking_review_count` as reviewcount,`hotel_booking_review_url` as reviewurl,( 6371 * acos( cos( radians( '.$lat.' ) ) * cos( radians( hotel_latitude ) ) * cos( radians( hotel_longitude ) - radians( '.$long.' ) ) + sin( radians( '.$lat.') ) * sin( radians( hotel_latitude ) ) ) ) AS distance,created_date from '.$tbl.' LOWER(`hotel_name`) Like Lower("%'.$name.'%")';
    
           $response = mysql_query($getData);
           $check  = mysql_num_rows($response);
            if($check > 0){
			   while($row = mysql_fetch_array($response)){
				  $id = $row['hotel_id'] != '' ? $row['hotel_id'] : 'N/A' ;
				   $name = $row['hotel_name'] != '' ? $row['hotel_name'] : 'N/A';
				   $Lat = $row['hotel_latitude'] != '' ? $row['hotel_latitude'] : 'N/A';
				   $Long = $row['hotel_longitude'] != '' ? $row['hotel_longitude'] : 'N/A';
				   $rate = $row['rating'] != '' ? $row['rating'] : 0;
				   $reviewCount = $row['reviewcount'] != '' ? $row['reviewcount'] : 0;
				   $reviewUrl = $row['reviewurl'] != '' ? $row['reviewurl'] : 'N/A';
				   $ratingScale = '10.0';
			   $date = date('Y-m-d\TH:i:sP',strtotime($row['created_date'])); 
					  $booking_data = array(
										'hotel_id'=>$id,
										'hotel_name'=>$name,
										'latitude'=>$Lat,
										'Longitude'=>$Long,
										'rating'=>$rate,
										'reviewcount'=>$reviewCount,
										'reviewurl'=>$reviewUrl,
										'ratingScale'=>$ratingScale,
										'date'=>$date
									);
                }									
			}else{
				    $booking_data = array();
			}	
			return $booking_data;
		}		
				
	}
?>
</body>
</html>
