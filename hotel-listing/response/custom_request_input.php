<?php
/*
 *   FileName    : custom_request_input.php
 *
 *   FilePurpose : Take the xml request for searching
 *
 *   Created By  :  SIPL
 *
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Custom Request For Hotel Rating</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<script type="text/javascript">
	 function validate(){
		 var text = document.getElementById("xmldata").value;
		   if(text == ''){
			   alert("Please Insert XML Input First");
			   return false;
			}else{
				return true;	
			}	 
		}
	</script>
</head>
<style>
.tablebg{border: 1px dashed #8C8C8C; background:#E2E2E2; padding: 10px;font-family:arial;}
.tablebg th{ background: none repeat scroll 0 0 #D0D0D0;
    border: 1px solid #BCBCBC;
 margin-bottom: 10px;
    padding: 15px 0;
    width: 100%;}
 .btn-submit input[type="submit"]{background: none repeat scroll 0 0 #D0D0D0;
    border: 1px solid #BCBCBC; padding: 4px 8px; cursor: pointer;}   
</style>
<body>
  <form name="" id="" method="post" action="custom_request_action.php">
	<table style="align:center;" class="tablebg" align="center" >
	<tr>
		 <th colspan="3">Insert Your XML Request Here:</th>
	</tr>	
	<tr><td>&nbsp;</td></tr>	
	  <tr>
		  <td>XML:</td>
		  <td><textarea name="xmldata" id="xmldata" rows="25" cols="35" placeholder="Enter your XML input here:"></textarea></td>
		  <td width="400" readonly="readonly"><?php echo "INPUT EXAMPLE : <br/><br/>". htmlentities("<?xml version='1.0' encoding='utf-8' ?>
<searchRatingRequest>
		<nameQuery>Hotel Viento 10</nameQuery>
	<geolocation>
	   <latitude>-4.769661</latitude>
	   <longitude>37.8814588</longitude>
	</geolocation>
		<searchRadius>500</searchRadius>
</searchRatingRequest>"); ?> 
	  </tr>
	  <tr>
		 <td colspan="3" class="btn-submit" align="right">
			 <input type="submit" name="submit" id="submit" onclick="return validate();" value="Submit">
		 </td> 
	  </tr>
	</table>
  </form>
</body>
</html>