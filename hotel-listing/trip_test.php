<?php
/*
 *   FileName    : trip_test.php
 *
 *   FilePurpose : Scrap the data from tripadvisor.in
 *
 *   Created By  :  SIPL
 *
*/

include('../includes/inc_config.php');

global $con, $res;

// Query to fetch all cities of a country //
  $query = "select A.country as country,A.ISO2 as countryCode,A.CurrencyCode as currencyCode,B.city from Countries as A LEFT JOIN  Cities as B ON A.CountryId = B.CountryId";
  $result = mysql_query($query,$con) or die('Problem in select query'.mysql_error());
  while($row = mysql_fetch_array($result)){
		// Taking various input parameter from query result //
		$country = $row['country'];
		$countryCode =  $row['countryCode'];
		$currencyCode = $row['currencyCode'];
		$city = $row['city'];
                if($country != '' && $countryCode != '' &&  $currencyCode != '' && $city != '')
		   import($country,$countryCode,$currencyCode,$city,''); // Calling function to get detail of given input //
   }


// Defination of import function //
function import($country,$countryCode,$currencyCode,$city,$destinationId){
	global $con, $res;
	$selectedCountry = $country;
	if($destinationId !=''){
		$query = "select CurrencyCode from Countries where  country='".$country."'";
		$result = mysql_query($query,$con) or die('Problem in select query'.mysql_error());
		if($row = mysql_fetch_array($result)){
			$currencyCode = $row['CurrencyCode'];
		}
	}
	$apiKey = '2kmwxzue3m8uqwk2y8a3xca7';  
	$secret = '';  
	$timestamp = gmdate('U'); 
	$sig = md5($apiKey . $secret . $timestamp);
	
	$url  ='http://api.eancdn.com/ean-services/rs/hotel/v3/list?cid=55505';
	$url .= '&minorRev=99';
	$url .= '&apiKey=2kmwxzue3m8uqwk2y8a3xca7';
	$url .= '&locale=en_US';
	$url .= '&currencyCode='.$currencyCode;
	
	$XML = "&xml=<HotelListRequest>
	 if(".$destinationId." !=''){
		<destinationId>".$destinationId."</destinationId>
	  }
	  <city>".$city."</city>
	  <countryCode>".$countryCode."</countryCode>
	  <RoomGroup>
	    <Room>
	        <numberOfAdults>2</numberOfAdults>
	    </Room>
	  </RoomGroup>
	   <numberOfResults>25</numberOfResults>
	 </HotelListRequest>";  
	
	$mainUrl = $url.$XML;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,"xmlRequest=" . $XML);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);
                $array_data = json_decode($data,true);
                $todays_date = date("Y-m-d H:i:s"); 

        if(!empty($array_data) && count($array_data)>0){	
	   foreach($array_data as $data){
		if(isset($data['EanWsError'])){
                    if(isset($data['LocationInfos']['LocationInfo'])){
				for($i=0;$i<count($data['LocationInfos']['LocationInfo']);$i++){
					$destinationId = $data['LocationInfos']['LocationInfo'][$i]['destinationId'];
					$city = $data['LocationInfos']['LocationInfo'][$i]['city'];
					$countryCode = $data['LocationInfos']['LocationInfo'][$i]['countryCode'];
					$countryName = $data['LocationInfos']['LocationInfo'][$i]['countryName'];
					import($countryName,$countryCode,'',$city,$destinationId);
				}
			}
		}else{
                   	$hotelId = '';$hotelName ='';$address='';$city='';$postalCode = '';$hotelRating = 0;$confidenceRating = 0;$tripRating = 0;$tripReviewCount =  0;$tripReviewUrl = ''; $latitude = '';$longitude ='';$thumbnail ='';
			if(isset($data['HotelList']['HotelSummary'])){
				for($i=0;$i < count($data['HotelList']['HotelSummary']);$i++){
					if(isset($data['HotelList']['HotelSummary'][$i]['hotelId'])){
						$hotelId =  $data['HotelList']['HotelSummary'][$i]['hotelId'];
					}
					
					if(isset($data['HotelList']['HotelSummary'][$i]['name'])){		
						$hotelName =$data['HotelList']['HotelSummary'][$i]['name'];
					}
					
					if(isset($data['HotelList']['HotelSummary'][$i]['address1'])){		
						$address = $data['HotelList']['HotelSummary'][$i]['address1'];
					}
					
					if(isset($data['HotelList']['HotelSummary'][$i]['city'])){	
						$city = $data['HotelList']['HotelSummary'][$i]['city'];
					}
						
					$selectedCountry =  $selectedCountry;
					
					if(isset($data['HotelList']['HotelSummary'][$i]['postalCode'])){
						$postalCode = $data['HotelList']['HotelSummary'][$i]['postalCode'];
					}	
					
					if(isset($data['HotelList']['HotelSummary'][$i]['hotelRating'])){
					  $hotelRating = $data['HotelList']['HotelSummary'][$i]['hotelRating'];
					}
					
					if(isset($data['HotelList']['HotelSummary'][$i]['confidenceRating'])){	
							$confidenceRating = $data['HotelList']['HotelSummary'][$i]['confidenceRating'];
				    }
					
					if(isset($data['HotelList']['HotelSummary'][$i]['tripAdvisorRating'])){		
						$tripRating =  $data['HotelList']['HotelSummary'][$i]['tripAdvisorRating'];
					}
							
					if(isset($data['HotelList']['HotelSummary'][$i]['tripAdvisorReviewCount'])){
						$tripReviewCount =  $data['HotelList']['HotelSummary'][$i]['tripAdvisorReviewCount'];
					}		
					
					if(isset($data['HotelList']['HotelSummary'][$i]['tripAdvisorRatingUrl'])){
						$tripReviewUrl =  $data['HotelList']['HotelSummary'][$i]['tripAdvisorRatingUrl'];
					}
				
					if(isset($data['HotelList']['HotelSummary'][$i]['latitude'])){		
						$latitude = $data['HotelList']['HotelSummary'][$i]['latitude'];
					}	
				
					if(isset($data['HotelList']['HotelSummary'][$i]['longitude'])){
						$longitude = $data['HotelList']['HotelSummary'][$i]['longitude'];
					}	
					
					if(isset($data['HotelList']['HotelSummary'][$i]['thumbNailUrl'])){
						$thumbnail = $data['HotelList']['HotelSummary'][$i]['thumbNailUrl'];
					}
                                        
                                      if($hotelId != ''){
                                             mysql_query("SET NAMES utf8"); 
					     mysql_query("SET CHARACTER_SET utf8"); 
					     mysql_query("SET COLLATION_CONNECTION = 'utf-8'");
					     // Condition to check hotel_id already exist or not //
					      $query1 = "select hotel_name from tbl_trip_hotels where hotel_id=".$hotelId."";
					      $response = mysql_query($query1);
						if($response){
							$result1 = mysql_num_rows($response);
							if($result1 <= 0){
                                                        	$query = 'INSERT INTO tbl_trip_hotels(`hotel_id`,`hotel_name`,`hotel_address`,`hotel_city`,`hotel_country`,`hotel_postal_code`,`hotel_rating`,`hotel_confidence_rating`,`trip_advisor_rating`,`trip_advisor_review_count`,`trip_advisor_review_url`,`hotel_latitude`,`hotel_longitude`,`hotel_thumbnail_url`,`created_date`) values ("'.$hotelId.'","'.$hotelName.'","'.$address.'","'.$city.'","'.$selectedCountry.'","'.$postalCode.'","'.$hotelRating.'","'.$confidenceRating.'","'.$tripRating.'","'.$tripReviewCount.'","'.$tripReviewUrl.'","'.$latitude.'","'.$longitude.'","'.$thumbnail.'","'.$todays_date.'")';
                                                                $result = mysql_query($query,$con) or die('Problem in select query'.mysql_error());
                                                                if($result){
                                                                             echo "Inserted Successfully";	
                                                                            }
                                                         }
                                                }
			             }
		             }
			}
		}
	}
    }
}
//************ END OF FILE **********************//
?>